import os

from RPA.Browser.Selenium import Selenium
from RPA.FileSystem import FileSystem

from agency import AgenciesService, Browser
from utils.configs import get_config


def main():
    url = "https://itdashboard.gov"
    dive_in_to_agency_btn = "class:btn"
    agency_export_dir = os.path.abspath("output")
    agency_locator = "id:agency-tiles-widget"
    agency_xlsx = "Agencies.xlsx"
    agency_sheet = "Individual Investments"
    agency_handler = get_config("config.ini")
    try:
        web = Browser(Selenium())
        web.open_website(url)
        agency = AgenciesService(
            agency_xlsx,
            agency_sheet,
            agency_export_dir,
            FileSystem(),
            agency_handler
        )
        agency.handle_agencies(agency_locator, dive_in_to_agency_btn, web)
    finally:
        web.close_website()


if __name__ == "__main__":
    main()
