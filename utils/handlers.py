from typing import (
    List, Dict,
)

from RPA.Excel.Files import Files


class ExcelHandler:
    def __init__(self, export_dir: str):
        self.lib: Files = Files()
        self.export_dir = export_dir

    def make_excel(self, title: str, data: List[Dict] = None):
        self.lib.create_workbook(f"{self.export_dir}/{title}")
        self.make_sheet(title, data)

    def make_sheet(self, title: str, data: List[Dict] = None):
        if data:
            self.lib.create_worksheet(title, data)
        else:
            self.lib.create_worksheet(title)
        self.lib.save_workbook()
