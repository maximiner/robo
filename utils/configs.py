import configparser


def get_config(conf_file: str) -> str:
    config = configparser.ConfigParser()
    config.read(conf_file)
    return config["main"]["handle_agency_title"]
