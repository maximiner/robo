import os
import re

from datetime import timedelta
from typing import (
    List, Dict, Union, Optional
)

from RPA.Browser.Selenium import Selenium
from RPA.FileSystem import FileSystem
from RPA.PDF import PDF
from selenium.common.exceptions import WebDriverException

from utils.handlers import ExcelHandler


class Browser:
    def __init__(
            self,
            browser: Selenium):
        self.browser: Selenium = browser

    def open_website(self, url: str):
        self.browser.open_available_browser(url)

    def download_to_dir(self, directory: str):
        self.browser.set_download_directory(directory)

    def close_website(self):
        self.browser.close_browser()


class AgenciesService:
    def __init__(
            self,
            title: str,
            investments_table: str,
            export: str,
            file_system: FileSystem,
            agency_detail: str = "National Science Foundation"

    ):
        self.title = title
        self.investments_table = investments_table
        self.export_dir = export
        self.file_system: FileSystem = file_system
        self.excel: ExcelHandler = ExcelHandler(self.export_dir)
        self.agency_id = agency_detail
        self.pdf = PDF()

    def handle_agencies(self, locator: str, btn: str, web: Browser):
        web.browser.wait_until_page_contains_element(btn)
        web.browser.click_element(btn)
        amounts, links = self.parse_agencies(locator, web)
        self.excel.make_excel(self.title, amounts)
        self.handle_agency_links(links, self.agency_id)

    def parse_agencies(self, locator: str, web: Browser):
        web.browser.wait_until_element_is_visible(locator)
        agency_tiles = web.browser.find_element(locator)

        amounts: List[Dict] = list()
        links: List[Dict] = list()
        agency_tiles_locator = "tuck-5"
        title_locator = "w200"
        amount_locator = "w900"
        btn_locator = "btn"

        items = agency_tiles.find_elements_by_class_name(
            agency_tiles_locator)
        for item in items:
            title = item.find_element_by_class_name(title_locator).text
            amounts.append({
                'title': title,
                'amount': item.find_element_by_class_name(
                    amount_locator).text,
            })
            links.append({
                'title': title,
                'link': item.find_element_by_class_name(
                    btn_locator).get_property('href'),
            })
        return amounts, links

    def handle_agency_links(self, links: List[Dict], agency):
        locator = "name:investments-table-object_length"
        item_length = "All"
        investments_table: List[Dict] = list()
        for item in links:
            if agency == item['title']:
                try:
                    web = Browser(Selenium())
                    web.download_to_dir(self.export_dir)
                    web.open_website(item['link'])
                    investments_table += self.handler_agency_investments(
                        locator, item_length, web
                    )
                finally:
                    web.close_website()

        self.excel.make_sheet(self.investments_table, investments_table)

    def handler_agency_investments(
            self,
            locator: str,
            label: str,
            web: Browser
    ) -> List[Dict]:
        web.browser.wait_until_page_contains_element(
            locator, timedelta(seconds=40))
        object_length = web.browser.find_element(locator)
        option_locator = "option"
        investment_table_locator = "id:investments-table-object_wrapper"
        options = object_length.find_elements_by_tag_name(option_locator)
        for option in options:
            if option.text == label:
                option.click()

        all_check_locator = "css:div#investments-table-object_paginate span"
        web.browser.wait_until_element_does_not_contain(
            all_check_locator, "2", timedelta(seconds=40))
        return self.parse_investments_table(investment_table_locator, web)

    def parse_investments_table(
            self,
            locator: str,
            web: Browser
    ) -> List[Dict]:
        web.browser.wait_until_element_is_visible(locator)
        tables = web.browser.find_element(locator)
        head_locator = "dataTables_scrollHead"
        head_th_locator = "th"
        rows_locator = "investments-table-object"
        rows_tbody_locator = "tbody"
        rows_tr_locator = "tr"
        rows_td_locator = "td"
        link_pdf_locator = "a"
        btn_pdf_locator = "id:business-case-pdf"
        heads_table = tables.find_element_by_class_name(head_locator)
        heads = heads_table.find_elements_by_tag_name(head_th_locator)
        investments_table: List[Dict] = list()
        head_items: List = list()
        for head in heads:
            head_items.append(head.text)

        row_table = tables.find_element_by_id(rows_locator)
        row_body = row_table.find_element_by_tag_name(rows_tbody_locator)
        rows = row_body.find_elements_by_tag_name(rows_tr_locator)
        links_pdf: List[Dict] = list()
        for row in rows:
            values = row.find_elements_by_tag_name(rows_td_locator)
            row_item: Dict = dict()
            for index, value in enumerate(values):
                row_item.update({head_items[index]: value.text})

            link_in_row = self.parse_pdf_links(row, link_pdf_locator)
            if link_in_row:
                link_in_row.update({"title": row_item["Investment Title"]})
                links_pdf.append(link_in_row)
            investments_table.append(row_item)

        self.handler_investment(btn_pdf_locator, links_pdf)
        return investments_table

    def parse_pdf_links(self, element, locator):
        try:
            a_tag = element.find_element_by_tag_name(locator)
        except WebDriverException as err:
            a_tag = None
        finally:
            if a_tag:
                item = {
                    "uii": a_tag.text, "link": a_tag.get_property("href")
                }
                return item
            return None

    def handler_investment(
            self,
            locator: str,
            links: List[Dict]
    ):
        for item in links:
            try:
                web = Browser(Selenium())
                web.download_to_dir(self.export_dir)
                web.open_website(item["link"])
                self.download_investments_pdf(
                    locator, item["uii"], web)
            finally:
                web.close_website()
                self.parse_pdf_files(item["uii"], item["title"])

    def download_investments_pdf(
            self,
            btn: str,
            filename: str,
            web: Browser
    ):
        web.browser.wait_until_page_contains_element(btn)
        self.delete_exists_file(filename)
        web.browser.click_element(btn)
        self.file_system.wait_until_created(
            f"{self.export_dir}/{filename}.pdf",
            timeout=60*10
        )

    def delete_exists_file(self, filename: str):
        pdf_files = f"{self.export_dir}/{filename}.pdf"
        if os.path.isfile(pdf_files):
            os.remove(pdf_files)

    def parse_pdf_files(self, uii: str, title: str):
        filepath = f"{self.export_dir}/{uii}.pdf"
        text = self.pdf.get_text_from_pdf(filepath)
        name: str = ''
        uii: str = ''
        for key, value in text.items():
            match_obj = re.search(
                "1. Name of this Investment\: ([\n\d()A-Za-z \,\-]+).*2."
                " Unique Investment Identifier .UII.: ([\d\- ]+)",
                value,
                re.DOTALL
            )
            if match_obj is None or match_obj.group(2) is None:
                print(f"From {filepath} not found name or uii")
            name = match_obj.group(1).strip()
            uii = match_obj.group(2).strip()
            if title != name:
                print(f"From {filepath} {title} and {name} not equal")
            break

        return name, uii
